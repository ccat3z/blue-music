package krrxue;

import krrxue.util.LineProcess;
import krrxue.util.Log;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class MusicService extends Thread {
    private ArrayList<SingleService> serviceArrayList;
    private MusicServiceListener musicServiceListener;
    private ServerSocket server;
    private Library library;

    public static final int COMMAND_PORT = 1344;
    public static final int OBJECT_PORT = 2424;

    private boolean runing = true;

    public static final String PLAY_COMMAND =         "PLAY___________";
    public static final String PLAYLIST_COMMAND =     "LIST___________";
    public static final String VOLUME_COMMAND =       "VOLUME_CHANGE__";
    public static final String ONASK_PLAY_COMMAND =       "ASK_PLAY_______";
    public static final String ONASK_PLAYLIST_COMMAND =   "ASK_LIST_______";
    public static final String ONPLAY_NEXT_COMMAND =      "PLAY_NEXT______";
    public static final String ONPLAY_PRE_COMMAND =       "PLAY_PRE_______";
    public static final String ONPAUSE_COMMAND =          "PAUSE__________";
    public static final String ONVOLUMEUP_COMMAND =       "VOLUME UP______";
    public static final String ONVOLUMEDOWN_COMMAND =     "VOLUME DOWN____";
    public static final String ONPLAY_COMMAND =           "PLSONG_________";

    public MusicService(Library library, MusicServiceListener musicServiceListener){
        this.library = library;
        this.musicServiceListener = musicServiceListener;
        serviceArrayList = new ArrayList<SingleService>();
    }

    @Override
    public void run() {
        try {
            server = new ServerSocket(COMMAND_PORT);
            while(runing) {
                Socket client = server.accept();
                SingleService service = new SingleService(client);
                serviceArrayList.add(service);
                service.start();
            }
        } catch (Exception e) {
            if(! e.getMessage().equals("Socket closed")) e.printStackTrace();
        }
    }

    private void sendCommand(String s){
        for (SingleService singleService : serviceArrayList){
            singleService.sendCommand(s);
        }
    }

    public void play(Song song){
        sendCommand(PLAY_COMMAND + song.getFile());
    }

    public void playlist(PlayList pl){
        sendCommand(PLAYLIST_COMMAND + pl.getName());
    }

    public void kill(){
        runing = false;
        try {
            for (SingleService singleService : serviceArrayList){
                singleService.kill();
            }
            server.close();
        } catch (Exception e) {

        }
    }

    private class SingleService extends Thread {
        private LineProcess lineProcess;
        private Socket socket;
        private OutputStream outputStream;
        private InputStream inputStream;

        private SingleService(Socket socket)
        {
            ServerSocket objectSocketServer = null;
            Socket objectSocket = null;
            Log.debug("[Service] new connect");
            this.socket = socket;
            try {
                outputStream = socket.getOutputStream();
                inputStream = socket.getInputStream();
                lineProcess = new LineProcess(inputStream);

                //send library object
                Log.debug("[Service] send:library");
                objectSocketServer = new ServerSocket(OBJECT_PORT);
                objectSocket = objectSocketServer.accept();
                library.saveToStream(objectSocket.getOutputStream());

                lineProcess.registerListener(ONASK_PLAY_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        play(musicServiceListener.onAskPlay());
                    }
                });
                lineProcess.registerListener(ONASK_PLAYLIST_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        playlist(musicServiceListener.onAskPlayList());
                    }
                });
                lineProcess.registerListener(ONPLAY_NEXT_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.playNext();
                    }
                });
                lineProcess.registerListener(ONPLAY_PRE_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.playPre();
                    }
                });
                lineProcess.registerListener(ONPAUSE_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.pause();
                    }
                });
                lineProcess.registerListener(ONVOLUMEUP_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.onVolumeUp();
                    }
                });
                lineProcess.registerListener(ONVOLUMEDOWN_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.onVolumeDown();
                    }
                });
                lineProcess.registerListener(ONPLAY_COMMAND, new LineProcess.MessageListener() {
                    @Override
                    public void handle(String msg) {
                        Log.debug("[Service] get:" + msg);
                        musicServiceListener.onPlay(msg.substring(ONPLAY_COMMAND.length()));
                    }
                });
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                try {
                    objectSocket.close();
                    objectSocketServer.close();
                } catch (Exception e) {

                }
            }
        }

        public void run()
        {
            try {
                lineProcess.run();
                socket.close();
                serviceArrayList.remove(this);
                Log.debug("[Service] disconnect");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void kill(){
            try {
                socket.close();
            } catch (Exception e) {

            }
        }

        private void sendCommand(String command){
            Log.debug("[Service] send:" + command);
            new PrintStream(outputStream).println(command);
        }
    }

    public interface MusicServiceListener {
        Song onAskPlay();
        PlayList onAskPlayList();
        void playNext();
        void playPre();
        void pause();
        void onVolumeUp();
        void onVolumeDown();
        void onPlay(String file);
    }
}
