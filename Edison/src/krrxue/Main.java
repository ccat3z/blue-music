package krrxue;

import krrxue.util.Log;
import sun.misc.Signal;
import sun.misc.SignalHandler;

public class Main implements MusicService.MusicServiceListener, MPlayer.MPlayerListener{
    private MPlayer mPlayer;
    private Library library;
    private MusicService musicService;

    public Main(final String music, String config){
        mPlayer = new MPlayer(this);
        mPlayer.setMode(MPlayer.MODE_SINGLE);
        Signal.handle(new Signal("INT"), new SignalHandler() {
            @Override
            public void handle(Signal signal) {
                mPlayer.quit();
            }
        });

        try {
            library = new Library(music, config);
            musicService = new MusicService(library, this);
            if (!mPlayer.playList(library.getLibrary())) Log.info("PLAY FAILED");
            musicService.start();
        } catch (Exception e) {
            e.printStackTrace();
            mPlayer.quit();
        }

        mPlayer.waitFor();
        musicService.kill();
        if (library != null) library.saveToFile();
    }

    public static void main (String args[]){
        new Main(args[0],args[1]);
        Log.info("Bye bye~ `^`");
    }

    @Override
    public void onMPlayerChangePlayList(PlayList pl) {
        Log.debug("PLAYLIST:" + pl.getName());
    }

    @Override
    public void onMPlayerChangeSong(Song song) {
        Log.info("[PLAY]" + song.getName());
        musicService.play(song);
    }

    @Override
    public Song onAskPlay() {
        return mPlayer.getCurrentSong();
    }

    @Override
    public PlayList onAskPlayList() {
        return mPlayer.getCurrentPlayList();
    }

    @Override
    public void playNext() {
        mPlayer.playNext(true);
    }

    @Override
    public void playPre() {
        mPlayer.playPre(true);
    }

    @Override
    public void onVolumeUp() {
        mPlayer.volumeUp();
    }

    @Override
    public void onVolumeDown() {
        mPlayer.volumeDown();
    }

    @Override
    public void pause() {
        mPlayer.pause();
    }

    @Override
    public void onPlay(String file) {
        mPlayer.play(library.getLibrary().indexOfSongByFile(file));
    }
}