package krrxue;

import krrxue.util.LineProcess;
import krrxue.util.Log;

import java.io.PrintStream;

class MPlayer {
    private Process mplayerProcess;
    private PrintStream in;
    private LineProcess lineProcess;

    private MPlayerListener mPlayerListener;

    private PlayList currentPlayList;
    private int playListIndex;
    private int mode = MODE_LOOP;

    private Song currentSong;

    public static final int MODE_LOOP = 0;
    public static final int MODE_SINGLE = 1;
    public static final int MODE_RANDOM = 2;

    public MPlayer(MPlayerListener mPlayerListener) {
        this.mPlayerListener = mPlayerListener;
        try {
            mplayerProcess = Runtime.getRuntime().exec("mplayer -slave -quiet -idle -msglevel all=6");
        } catch (Exception e){
            e.printStackTrace();
        }
        in = new PrintStream(mplayerProcess.getOutputStream());
        lineProcess = new LineProcess(mplayerProcess.getInputStream());
        lineProcess.start();
        lineProcess.registerListener("EOF code: 1", new LineProcess.MessageListener() {
            @Override
            public void handle(String msg) {
                onEof();
            }
        });
    }

    private void command(String cmd) {
        in.println(cmd);
        in.flush();
    }

    private void play(Song song) {
        command("loadfile " + "\"" + song.getFile() + "\"");
    }

    public boolean playList(PlayList pl) throws Exception{
        return playList(pl, 0);
    }

    public void play(int index){
        if (index >= 0 & index < currentPlayList.size()){
            playListIndex = index;
            currentSong = currentPlayList.get(index);
            play(currentSong);
            mPlayerListener.onMPlayerChangeSong(currentSong);
        } else if (index < 0){
            play(index + currentPlayList.size());
        } else {
            play(index - currentPlayList.size());
        }
    }

    public boolean playList(PlayList pl, int index){
        if (pl == null) return false;
        if (pl.size() == 0) return false;
        currentPlayList = pl;
        mPlayerListener.onMPlayerChangePlayList(pl);
        play(index);
        return true;
    }

    public void setMode(int m){
        if(m == MODE_LOOP | m == MODE_SINGLE | m == MODE_RANDOM){
            mode = m;
        }
    }

    public void playNext(){
        switch (mode){
            case MODE_LOOP:
                play(++playListIndex);
                break;
            case MODE_SINGLE:
                play(playListIndex);
                break;
            case MODE_RANDOM:
                play((int) (Math.random() * currentPlayList.size()));
                break;
        }
    }

    public void playNext(boolean force){
        if (force) play(++playListIndex);
    }

    public void playPre(){
        switch (mode){
            case MODE_LOOP:
                play(--playListIndex);
                break;
            case MODE_SINGLE:
                play(playListIndex);
                break;
            case MODE_RANDOM:
                play((int) (Math.random() * currentPlayList.size()));
                break;
        }
    }

    public void pause() {
        command("pause");
    }

    public void volumeUp(){
        command("volume +1");
    }

    public void volumeDown(){
        command("volume -1");
    }

    public void playPre(boolean force){
        if (force) play(--playListIndex);
    }

    public void quit() {
        command("quit");
    }

    public void waitFor() {
        try {
            mplayerProcess.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onEof() {
        playNext();
    }

    public PlayList getCurrentPlayList() {
        return currentPlayList;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public interface MPlayerListener {
        void onMPlayerChangeSong(Song song);
        void onMPlayerChangePlayList(PlayList pl);
    }
}
