package krrxue.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class LineProcess extends Thread {
    private InputStream in;
    private Map<String, MessageListener> msgMap;

    public LineProcess(InputStream in){
        this.in = in;
        msgMap = new HashMap<String, MessageListener>();
    }

    @Override
    public void run() {
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                Log.debug(line);
                for (String msg : msgMap.keySet()){
                    if (line.matches(msg+".*")){
                        msgMap.get(msg).handle(line);
                    }
                }
            }
            reader.close();
        } catch (Exception e){

        }
    }

    public void registerListener(String msg, MessageListener listener){
        msgMap.put(msg, listener);
    }

    public void unregisterListener(String msg){
        msgMap.remove(msg);
    }

    public interface MessageListener{
        void handle(String msg);
    }
}
