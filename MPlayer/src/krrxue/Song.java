package krrxue;

import krrxue.util.LineProcess;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

class Song implements Serializable{
    private String name;
    private String album;
    private String artist;
    private String length;
    private String file;
    private Set<String> playListSet;

    public final static String NAME_FLAG = "MEDIAINFONAME";
    public final static String ALBUM_FLAG = "MEDIAINFOALBUM";
    public final static String ARTIST_FLAG = "MEDIAINFOARTIST";
    public final static String LENGTH_FLAG = "MEDIAINFOLENGTH";

    static Song getSong(String file){
        Song song = new Song(file);
        return (song.getName().length() != 0) ? song : null;
    }

    private Song(String file){
        playListSet = new HashSet<String>();
        this.file = file;
        getMediaInfo();
    }

    private void getMediaInfo(){
        try {
            Process mediainfoProcess = Runtime.getRuntime().exec(new String[]{
                    "mediainfo",
                    "--Output=" + "General;" +
                    NAME_FLAG + "%Track%\\n" +
                    ALBUM_FLAG + "%Album%\\n" +
                    ARTIST_FLAG + "%Performer%\\n" +
                    LENGTH_FLAG + "%Duration%",
                    file});
            LineProcess lineProcess = new LineProcess(mediainfoProcess.getInputStream());
            lineProcess.registerListener(NAME_FLAG, new LineProcess.MessageListener() {
                @Override
                public void handle(String msg) {
                    name = msg.substring(NAME_FLAG.length());
                }
            });
            lineProcess.registerListener(ALBUM_FLAG, new LineProcess.MessageListener() {
                @Override
                public void handle(String msg) {
                    album = msg.substring(ALBUM_FLAG.length());
                }
            });
            lineProcess.registerListener(ARTIST_FLAG, new LineProcess.MessageListener() {
                @Override
                public void handle(String msg) {
                    artist = msg.substring(ARTIST_FLAG.length());
                }
            });
            lineProcess.registerListener(LENGTH_FLAG, new LineProcess.MessageListener() {
                @Override
                public void handle(String msg) {
                    length = msg.substring(LENGTH_FLAG.length());
                }
            });
            lineProcess.run();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    public String getLength() {
        return length;
    }

    public String getFile() {
        return file;
    }

    public boolean addPlaylist(String playlistName){
        return playListSet.add(playlistName);
    }

    public boolean removePlaylist(String playlistName){
        return playListSet.remove(playlistName);
    }

    public Set<String> getPlayListSet() {
        return playListSet;
    }

    @Override
    public String toString() {
        return name + ":" + album + ":" + artist + ":" + length + ":" +file;
    }

    @Override
    public boolean equals(Object o) {
        return ((Song) o).getFile().equals(this.file);
    }
}
