package krrxue;

import krrxue.util.Log;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class Library {
    private File musicDir;
    private File configFile;
    private PlayList library;
    private ArrayList<PlayList> playListArrayList;

    public final static String LIBRARY_PLAYLIST_NAME = "LIBRARY";
    public final static String MUSICDIR_EXCEPTION = "cant find music dir";
    public final static String CONFIGDIR_EXCEPTION = "cant reach config file";

    private Library() {

    }

    public Library (String dir, String config) throws Exception {
        this(new File(dir), new File(config));
    }

    public Library (File dir, File config) throws Exception {
        musicDir = dir;
        configFile = config;

        if (!musicDir.isDirectory()) throw new Exception(MUSICDIR_EXCEPTION);

        Log.debug("LOADING Library");
        if ((library = PlayList.loadFromFile(config.getPath())) == null){
            try {
                library = new PlayList(musicDir, LIBRARY_PLAYLIST_NAME);
                config.createNewFile();
                saveToFile();
            } catch (Exception e){
                throw new Exception(CONFIGDIR_EXCEPTION);
            }
        } else {
            library.upDateList(musicDir);
            saveToFile();
        }
        freshPlayLists();
        for (Song s : library){
            Log.debug(s.toString());
        }
        Log.debug("LOADING Library done");
    }


    //save and load
    public static Library loadLibraryFromStream (InputStream inputStream) throws Exception{
        Library library = new Library();

        library.library = PlayList.loadFromStream(inputStream);
        if (library.library != null) {
            library.freshPlayLists();
            return library;
        } else {
            return null;
        }
    }

    public boolean saveToStream (OutputStream outputStream) {
        return library.saveToStream(outputStream);
    }

    public void saveToFile(){
        library.saveToFile(configFile.getPath());
    }

    //library utils
    public void freshPlayLists() {
        playListArrayList = new ArrayList<PlayList>();
        for (Song song : library){
            for (String s : song.getPlayListSet()){
                boolean haveList = false;
                for (PlayList pl : playListArrayList){
                    if (pl.getName().equals(s)){
                        pl.add(song);
                        haveList = true;
                        break;
                    }
                }
                if (!haveList) {
                    PlayList pl = new PlayList(s);
                    pl.add(song);
                    playListArrayList.add(pl);
                }
                Log.debug("GOT PLAYLIST" + s);
            }
        }
    }

    //get playlist
    public ArrayList<PlayList> getPlayListArrayList() {
        return playListArrayList;
    }

    public PlayList getPlayListByName(String name) {
        for (PlayList pl : playListArrayList){
            if (pl.getName().equals(name)){
                return pl;
            }
        }
        return null;
    }

    public PlayList getLibrary() {
        return library;
    }


    //get song from library
    public Song getSongByName(String name){
        return getLibrary().get(getLibrary().indexOfSongByName(name));
    }

    public Song getSongByFile(String file){
        return getLibrary().get(getLibrary().indexOfSongByFile(file));
    }
}
