package krrxue;

import krrxue.util.Log;

import java.io.*;
import java.util.ArrayList;

class PlayList extends ArrayList<Song> {
    private String name;

    public static final String CANT_FIND_MUSIC_DIR_EXCEPTION = "CANT_FIND_MUSIC_DIR_EXPECTION";

    public PlayList(String name) {
        this.name = name;
    }

    public PlayList(File file, String name) throws Exception{
        this(name);
        try {
            for (File f : file.listFiles()) {
                if (addSongFromFile(f)) Log.debug("ADD " + f.getPath());
            }
        } catch (Exception e) {
            throw new Exception(CANT_FIND_MUSIC_DIR_EXCEPTION);
        }
    }


    //index of song
    public int indexOfSongByName(String name) {
        for(int i = 0; i < this.size(); i++){
            if(this.get(i).getName().equals(name)){
                return i;
            }
        }
        return -1;
    }

    public int indexOfSongByFile(String file) {
        for(int i = 0; i < this.size(); i++){
            if(this.get(i).getFile().equals(file)){
                return i;
            }
        }
        return -1;
    }


    //save and load
    public void upDateList(File file) throws Exception {
        try {
            ArrayList<String> files = new ArrayList<String>();
            ArrayList<Song> removeList = new ArrayList<Song>();
            for (File f : file.listFiles()) {
                files.add(f.getPath());
                if (this.indexOfSongByFile(f.getPath()) == -1) {
                    if (addSongFromFile(f))
                    Log.debug("ADD" + f.getPath());
                }
            }
            for (Song song : this){
                if (!files.contains(song.getFile())){
                    removeList.add(song);
                    Log.debug("DEL" + song.getFile());
                }
            }
            for (Song song : removeList){
                this.remove(song);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(CANT_FIND_MUSIC_DIR_EXCEPTION);
        }
    }


    //playlist utils
    private void addSongFromFile(String file){
        Song song = Song.getSong(new File(file).getPath());
        if (song != null) {
            this.add(song);
        }
    }

    private boolean addSongFromFile(File file){
        Song song = Song.getSong(file.getPath());
        if (song != null) {
            this.add(song);
            return true;
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }


    //save and load
    public boolean saveToFile(String file){
        try {
            saveToStream(new FileOutputStream(file));
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public static PlayList loadFromFile(String file){
        try {
            return loadFromStream(new FileInputStream(file));
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveToStream(OutputStream outputStream){
        try {
            ObjectOutputStream out = new ObjectOutputStream(outputStream);
            out.writeObject(this);
            out.close();
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public static PlayList loadFromStream(InputStream inputStream){
        try {
            ObjectInputStream in = new ObjectInputStream(inputStream);
            PlayList pl = (PlayList) in.readObject();
            in.close();
            return pl;
        } catch (Exception e){
            return null;
        }
    }
}
