package krrxue;

import com.amazon.kindle.kindlet.KindletContext;
import ixtab.jailbreak.Jailbreak;
import ixtab.jailbreak.SuicidalKindlet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;

public class Launch extends SuicidalKindlet implements MusicClient.MusicClientListener {
    PlayListUi playListUi;
    MusicClient musicClient;
    KindletContext context;
    JLabel volumeText;

    public void onCreate(final KindletContext context)
    {
        playListUi = new PlayListUi();
        playListUi.getNextButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.playNext();
            }
        });
        playListUi.getPreButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.playPre();
            }
        });
        playListUi.getPauseButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.pause();
            }
        });
        playListUi.getVolumeUpButtom().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.volumeUp();
            }
        });
        playListUi.getVolumeDownButtom().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.volumeDown();
            }
        });
        volumeText = playListUi.getVolumeText();
        this.context = context;
        context.getRootContainer().add(playListUi.getContainer());
    }

    @Override
    protected void onStart() {
        musicClient = new MusicClient("10.42.0.1",this,playListUi);
        musicClient.start();
    }

    @Override
    protected void onStop() {
        musicClient.kill();
    }

    protected Jailbreak instantiateJailbreak() {
        return new KJailbreak();
    }

    @Override
    public void onPlay(Song song) {
        onStatus(song.getName());
    }

    @Override
    public void onPlayList(PlayList pl) {
        //onStatus(pl.getName());
    }

    @Override
    public void onStatus(String str) {
        context.setSubTitle(str);
    }

    @Override
    public void onLoaded() {
        JPanel mplayListPanel = playListUi.getPlayListPanel();
        mplayListPanel.removeAll();
        final PlayList library = musicClient.getLibrary().getLibrary();
        for (Song s : library){
            SongUi songUi = new SongUi(s, musicClient);
            GridBagConstraints g = new GridBagConstraints();
            g.fill = GridBagConstraints.BOTH;
            g.gridwidth = 0;
            mplayListPanel.add(songUi.getMainPanel(), g);
        }
        mplayListPanel.repaint();
    }
}