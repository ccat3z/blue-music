package krrxue;

import javax.swing.*;

/**
 * Created by zlf on 6/7/16.
 */
public class PlayListUi {
    private JPanel mainPanel;
    private JButton preButton;
    private JButton pauseButton;
    private JButton nextButton;
    private JButton volumeUpButtom;
    private JButton volumeDownButtom;
    private JLabel volumeText;
    private JPanel playListPanel;

    public JPanel getContainer(){
        return mainPanel;
    }

    public JButton getNextButton() {
        return nextButton;
    }

    public JButton getPreButton() {
        return preButton;
    }

    public JButton getPauseButton() {
        return pauseButton;
    }

    public JButton getVolumeUpButtom() {
        return volumeUpButtom;
    }

    public JButton getVolumeDownButtom() {
        return volumeDownButtom;
    }

    public JLabel getVolumeText() {
        return volumeText;
    }

    public JPanel getPlayListPanel() {
        return playListPanel;
    }
}
