package krrxue;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import krrxue.util.LineProcess;

import javax.sound.sampled.Line;

public class MusicClient extends Thread{
    private BufferedReader reader;
    private MusicClientListener musicClientListener;
    private Library library;
    private Song currentSong;
    private PlayList currentPlaylist;
    private Socket socket;
    private String address;
    private OutputStream outputStream;
    private InputStream inputStream;
    private PlayListUi ui;

    //command
    public static final String ONPLAY_COMMAND =         "PLAY___________";
    public static final String ONPLAYLIST_COMMAND =     "LIST___________";
    public static final String ONVOLUME_COMMAND =       "VOLUME_CHANGE__";
    public static final String ASK_PLAY_COMMAND =       "ASK_PLAY_______";
    public static final String ASK_PLAYLIST_COMMAND =   "ASK_LIST_______";
    public static final String PLAY_NEXT_COMMAND =      "PLAY_NEXT______";
    public static final String PLAY_PRE_COMMAND =       "PLAY_PRE_______";
    public static final String PAUSE_COMMAND =          "PAUSE__________";
    public static final String VOLUMEUP_COMMAND =       "VOLUME UP______";
    public static final String VOLUMEDOWN_COMMAND =     "VOLUME DOWN____";
    public static final String PLAY_COMMAND =           "PLSONG_________";

    private static String COMMAND_LIST[] = {ONPLAY_COMMAND, ONPLAYLIST_COMMAND, ONVOLUME_COMMAND};

    public static final int COMMAND_PORT = 1344;
    public static final int OBJECT_PORT = 2424;

    public MusicClient(String address, final MusicClientListener musicClientListener, PlayListUi ui){
        this.musicClientListener = musicClientListener;
        this.address = address;
        this.ui = ui;
    }

    @Override
    public void run() {
        Socket objectSocket = null;
        try {
            //connect MusicService
            musicClientListener.onStatus("CONNECTING...");
            socket = new Socket(address, COMMAND_PORT);
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            musicClientListener.onStatus("CONNECTING...DONE");

            //get library object
            Thread.sleep(3000);
            musicClientListener.onStatus("LOADING LIBRARY...");
            objectSocket = new Socket(address, OBJECT_PORT);
            musicClientListener.onStatus("LOADING LIBRARY...CONNECTED");
            library = Library.loadLibraryFromStream(objectSocket.getInputStream());
            musicClientListener.onStatus("LOADING LIBRARY...DONE");
            musicClientListener.onStatus(Integer.toString(library.getLibrary().size()));

            askPlay();
            askPlayList();
            musicClientListener.onLoaded();

            //process command
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                for (String cmd : COMMAND_LIST) {
                    if (line.substring(0, cmd.length()).equals(cmd)) {
                        processCommand(cmd,line);
                    }
                }
            }
        } catch (Exception e) {
            musicClientListener.onStatus(e.getMessage());
        } finally {
            try {
                if (objectSocket != null) objectSocket.close();
                if (reader != null) reader.close();
            } catch (Exception e) {

            }
        }
    }

    public void kill() {
        try {
            if (socket != null) socket.close();
        } catch (Exception e) {

        }
    }

    public void askPlay() {
        sendCommand(ASK_PLAY_COMMAND);
    }

    public void askPlayList() {
        sendCommand(ASK_PLAYLIST_COMMAND);
    }

    public void playNext(){
        sendCommand(PLAY_NEXT_COMMAND);
    }

    public void playPre() {
        sendCommand(PLAY_PRE_COMMAND);
    }

    public void pause() {
        sendCommand(PAUSE_COMMAND);
    }

    public void play(Song s) {
        musicClientListener.onStatus("P");
        sendCommand(new String[] {PLAY_COMMAND,s.getFile()});
    }

    public void volumeUp(){
        sendCommand(VOLUMEUP_COMMAND);
    }

    public void volumeDown(){
        sendCommand(VOLUMEDOWN_COMMAND);
    }

    private void sendCommand(String command){
        new PrintStream(new PrintStream(outputStream)).println(command);
    }

    private void sendCommand(String commands[]) {
        PrintStream printStream =new PrintStream(new PrintStream(outputStream));
        for ( String s : commands) {
            printStream.print(s);
        }
        printStream.println();
    }

    private void processCommand(String cmd, String line){
        if (cmd.equals(ONPLAY_COMMAND)){
            musicClientListener.onPlay(library.getSongByFile(line.substring(ONPLAY_COMMAND.length())));
        } else if (cmd.equals(ONPLAYLIST_COMMAND)) {
            String name = line.substring(ONPLAYLIST_COMMAND.length());
            if (name.equals(Library.LIBRARY_PLAYLIST_NAME)) {
                musicClientListener.onPlayList(library.getLibrary());
            } else {
                musicClientListener.onPlayList(library.getPlayListByName(name));
            }
        }
    }

    public Library getLibrary() {
        return library;
    }

    public interface MusicClientListener {
        void onPlay(Song song);
        void onPlayList(PlayList pl);
        void onStatus(String str);
        void onLoaded();
    }
}
