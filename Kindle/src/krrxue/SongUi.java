package krrxue;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SongUi {
    private JPanel mainPanel;
    private JPanel songPanel;
    private JButton playButton;
    private JLabel songName;
    private JButton button1;
    private Song song;
    private MusicClient musicClient;

    public SongUi(Song s) {
        song = s;
        songName.setText(song.getName());
    }

    public SongUi(Song s, MusicClient m){
        this(s);
        musicClient = m;
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                musicClient.play(song);
            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JButton getPlayButton() {
        return playButton;
    }
}
